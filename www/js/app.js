// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('smartnotify', ['ionic'])

.controller('NotifiCtrl', function ($scope, $ionicModal, $ionicSideMenuDelegate,$http) {


//
        $scope.tasks = [
        { name:'Cookie Monster', title: 'cookie',date: '2016-11-10 15:17:36', message: "Don't delete your cookies! Eat them. om, nom, nom, nom, nom, nom, nom, nom!" },
        { name: 'Arin', title: 'work', date: '2016-12-11 16:17:36', message: 'like these sweet' },
        { name: 'Marty', title: 'The Future is here', date: '2100-12-10 15:17:36', message: "I'm from the future" },
        { name: 'Victor', title: 'Freze', date: '2010-12-10 15:17:36', message: "Ai am mistaah freeze, uarghhaa!" },
        /*{ name: 'Kat', title: 'intenrship', date: '2016-11-10 15:17:36', message: 'ewrrqwrqwesfdfsgdfhgfhjgfh' }, 
        { name: 'Kat', title: 'job', date: '2016-11-10 15:17:36', message: 'I feel the presence of the Almighty, who formed us in his own image, and the breath' },
        { name: 'Mabella', title: 'work', date: '2016-11-10 15:17:36', message: 'I feel the presence of the Almighty, who formed us in his own image, and the breath' },
        { name:'Stevie', title: 'exams',date: '2016-11-10 15:17:36', message: 'A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot' },
        { name: 'Alban', title: 'work', date: '2016-11-10 15:17:36', message: 'I feel the presence of the Almighty, who formed us in his own image, and the breath' },
        { name: 'Alban', title: 'job', date: '2016-11-10 15:17:36', message: 'I feel the presence of the Almighty, who formed us in his own image, and the breath' }*/
        ];      
        $scope.profiles = [
        { name: 'Name', img: 'https://placeholdit.imgix.net/~text?txtsize=18&bg=0033cc&txtclr=ffffff&txt=avatar.png&w=132&h=82&fm=jpg'},
        ]; 
              
$scope.sortType     = 'date'; // set the default sort type
$scope.sortReverse  = false;  // set the default sort order
$scope.searchFilter   = '';     // set the default search/filter term


// Create and load the Modal
$ionicModal.fromTemplateUrl('new-task.html', function(modal) {
    $scope.taskModal = modal;
}, {
    scope: $scope,
    animation: 'slide-in-up'
});
// current time
//http://stackoverflow.com/questions/22962468/angularjs-display-current-date
    $scope.date = new Date();  
// the way date is written confuses sort method
// Called when the form is submitted
$scope.createTask = function(task) {
    $scope.tasks.push({
        name: "You",
        title: task.title,
        date: Date(),
        message: task.msg
    });
    $scope.taskModal.hide();
    task.title = "";
    task.msg = "";
};

// Open our new task modal
$scope.newTask = function() {
    $scope.taskModal.show();
};

// Close the new task modal
$scope.closeNewTask = function() {
    $scope.taskModal.hide();
};
// Toggle Settings menu button
$scope.toggleLeftSideMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
};
    //delete
$scope.onItemDelete = function (task) {
    $scope.tasks.splice($scope.tasks.indexOf(task), 1);
};
    //open sigle message
$scope.openMessage = function (task) {
    var whereInTasks = $scope.tasks.indexOf(task)
    console.log("Where in tasks are we: ", whereInTasks);
    console.log($scope.tasks[whereInTasks].message)
    /*
    function expandTask(taskcompare) {
        return taskcompare.id === task.id;
    };
    console.log($scope.tasks.find(expandTask).message)*/
};

//http request, spytter fejl ud
/*

$scope.fetchMessage = function fetch(){
  
  $http.get('http://localhost:8000/users').
    success(function (data, status, headers, config) {
        $scope.Response = data;
    }).
    error(function (data, status, headers, config) {
        alert('error');
    });

  console.log('respose: ' + Response);
};*/
    

//andet forsøg

$scope.fetchMessage = function fetch(){
$http({
    method : "GET",
    url : "http://localhost:8000/users"
  })
  .then(function mySucces(response)
    {//when success
      apidata = $scope.myWelcome = response.data;
      angular.forEach(apidata, function(apidata, key)
      {
        //evt make a check if a message already exists (eg token when api offers one)
        var toPush = { 
          name: apidata.name,
          title: 'my password',
          date: apidata.created_at,
          message: 'this is my password: ' + apidata.password
          };

        $scope.tasks.push(toPush);
      });//for each løkke slut
      console.log("messages added");

    }//success end
  ,function myError(response)
    {
      $scope.myWelcome = "fejl: " + response.statusText;
    });
};
//http://stackoverflow.com/questions/20035101/no-access-control-allow-origin-header-is-present-on-the-requested-resource
//“No 'Access-Control-Allow-Origin' header is present on the requested resource”
//If I understood it right you are doing an XMLHttpRequest to a different domain than your page is on. So the browser is blocking it as it usually allows a request in the same origin for security reasons.

})
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
